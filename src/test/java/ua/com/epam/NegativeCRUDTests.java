package ua.com.epam;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ua.com.epam.entities.AuthorType;
import ua.com.epam.entities.CreateAuthorRequest;
import ua.com.epam.entities.DeleteAuthorRequest;
import ua.com.epam.entities.DeleteParams;

import static ua.com.epam.utils.Constant.*;

public class NegativeCRUDTests extends BaseTest {

    @Test(description = "Verify conflict for duplicated authors")
    public void checkDuplicatedAuthors() {
        AuthorType.AuthorName authorName = new AuthorType.AuthorName();
        authorName.setFirst(FIRSTNAME);
        authorName.setSecond(SECONDNAME);
        AuthorType.Birth birth = new AuthorType.Birth();
        birth.setCity(CITY);
        birth.setCountry(COUNTRY);
        birth.setDate(DATE);
        AuthorType authorType = new AuthorType()
                .setAuthorId(AUTHOR_ID)
                .setAuthorDescription(DESCRIPTION)
                .setAuthorName(authorName)
                .setBirth(birth)
                .setNationality(NATIONALITY);
        AuthorType createAuthorResponse = authorService
                .createAuthor(new CreateAuthorRequest().setAuthor(authorType));
        validation.validateCreateAuthor(authorType.getAuthorId(), createAuthorResponse.getAuthorId());
        try {
            authorService.createAuthor(new CreateAuthorRequest().setAuthor(authorType));
        } catch (Exception e) {
            validation.validateDuplicateAuthor(e.getMessage(), authorType.getAuthorId());
        }
    }

    @Test(description = "Check delete not exist author")
    public void checkDeleteNotExistAuthor() {
        DeleteAuthorRequest deleteAuthorRequest = new DeleteAuthorRequest();
        deleteAuthorRequest.setAuthorId(UNKNOWN_AUTHOR_ID);
        deleteAuthorRequest.setOptions(new DeleteParams().setForcibly(false));
        try {
            authorService.deleteAuthor(deleteAuthorRequest);
        } catch (Exception e) {
            validation.validateUnknownAuthor(e.getMessage(), UNKNOWN_AUTHOR_ID);
        }
    }

    @AfterClass
    public void deleteAuthors() {
        authorService.deleteAuthor(new DeleteAuthorRequest().setAuthorId(AUTHOR_ID));
    }
}

