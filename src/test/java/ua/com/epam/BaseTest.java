package ua.com.epam;

import ua.com.epam.services.AuthorService;
import ua.com.epam.validation.Validation;

public class BaseTest {

    protected Validation validation;
    protected AuthorService authorService;

    public BaseTest() {
        validation = new Validation();
        authorService = new AuthorService();
    }
}
