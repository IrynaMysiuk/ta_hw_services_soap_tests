package ua.com.epam.config;

public interface PropConstants {
    //API
    String API_PROTOCOL = "api.protocol";
    String API_HOST = "api.host";
    String API_PORT = "api.port";

}
