package ua.com.epam.utils;

public class Constant {
    public static final int AUTHOR_ID = 9659;
    public static final int AUTHOR_DELETE_ID = 9989;
    public static final int UNKNOWN_AUTHOR_ID = 0;
    public static final String FIRSTNAME = "Jeniffer";
    public static final String SECONDNAME = "fs";
    public static final String CITY = "sdf";
    public static final String COUNTRY = "sdfds";
    public static final String DATE = "2012-12-13";
    public static final String DESCRIPTION = "asdasd";
    public static final String NATIONALITY = "asd";
    public static final String SETDATE = "2011-11-12";
    public static final int SIZE = 5;
    public static final String DESC = "desc";
    public static final int PAGE = 1;
    public static final String ASC = "asc";
    private Constant() {

    }
}
