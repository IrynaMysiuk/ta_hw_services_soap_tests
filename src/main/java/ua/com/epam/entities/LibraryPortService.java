package ua.com.epam.entities;

import javax.xml.namespace.QName;
import javax.xml.ws.*;
import java.net.MalformedURLException;
import java.net.URL;

import static ua.com.epam.config.URI.BASE_URI;

@WebServiceClient(name = "LibraryPortService", targetNamespace = "libraryService", wsdlLocation = "http://localhost:8080/ws/library.wsdl")
public class LibraryPortService
        extends Service {

    private final static URL LIBRARYPORTSERVICE_WSDL_LOCATION;
    private final static WebServiceException LIBRARYPORTSERVICE_EXCEPTION;
    private final static QName LIBRARYPORTSERVICE_QNAME = new QName("libraryService", "LibraryPortService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL(BASE_URI + "/ws/library.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        LIBRARYPORTSERVICE_WSDL_LOCATION = url;
        LIBRARYPORTSERVICE_EXCEPTION = e;
    }

    public LibraryPortService() {
        super(__getWsdlLocation(), LIBRARYPORTSERVICE_QNAME);
    }

    public LibraryPortService(WebServiceFeature... features) {
        super(__getWsdlLocation(), LIBRARYPORTSERVICE_QNAME, features);
    }

    public LibraryPortService(URL wsdlLocation) {
        super(wsdlLocation, LIBRARYPORTSERVICE_QNAME);
    }

    public LibraryPortService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, LIBRARYPORTSERVICE_QNAME, features);
    }

    public LibraryPortService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public LibraryPortService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * @return returns LibraryPort
     */
    @WebEndpoint(name = "LibraryPortSoap11")
    public LibraryPort getLibraryPortSoap11() {
        return super.getPort(new QName("libraryService", "LibraryPortSoap11"), LibraryPort.class);
    }

    /**
     * @param features A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return returns LibraryPort
     */
    @WebEndpoint(name = "LibraryPortSoap11")
    public LibraryPort getLibraryPortSoap11(WebServiceFeature... features) {
        return super.getPort(new QName("libraryService", "LibraryPortSoap11"), LibraryPort.class, features);
    }

    private static URL __getWsdlLocation() {
        if (LIBRARYPORTSERVICE_EXCEPTION != null) {
            throw LIBRARYPORTSERVICE_EXCEPTION;
        }
        return LIBRARYPORTSERVICE_WSDL_LOCATION;
    }

}
