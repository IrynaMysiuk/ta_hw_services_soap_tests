package ua.com.epam;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ua.com.epam.entities.*;

import static ua.com.epam.utils.Constant.*;


public class CRUDAuthorTest extends BaseTest {

    @Test(description = "post single Author obj")
    public void checkPostAuthor() {

        AuthorType.AuthorName authorName = new AuthorType.AuthorName();
        authorName.setFirst(FIRSTNAME);
        authorName.setSecond(SECONDNAME);
        AuthorType.Birth birth = new AuthorType.Birth();
        birth.setCity(CITY);
        birth.setCountry(COUNTRY);
        birth.setDate(DATE);
        AuthorType authorType = new AuthorType()
                .setAuthorId(AUTHOR_ID)
                .setAuthorDescription(DESCRIPTION)
                .setAuthorName(authorName)
                .setBirth(birth)
                .setNationality(NATIONALITY);

        AuthorType createAuthorResponse = authorService.createAuthor(new CreateAuthorRequest().setAuthor(authorType));
        GetAuthorRequest authorRequest = new GetAuthorRequest();
        AuthorType authorResponse = authorService.getAuthor(authorRequest.setAuthorId(authorType.getAuthorId()));
        validation.validateCreateAuthor(authorType.getAuthorId(), createAuthorResponse.getAuthorId());
        validation.validateCreateAuthor(authorType.getAuthorId(), authorResponse.getAuthorId());
    }

    @Test(description = "Check delete single author")
    public void checkDeleteAuthor() {
        AuthorType.AuthorName authorName = new AuthorType.AuthorName();
        authorName.setFirst(FIRSTNAME);
        authorName.setSecond(SECONDNAME);
        AuthorType.Birth birth = new AuthorType.Birth();
        birth.setCity(CITY);
        birth.setCountry(COUNTRY);
        birth.setDate(SETDATE);
        AuthorType authorType = new AuthorType()
                .setAuthorId(AUTHOR_DELETE_ID)
                .setAuthorDescription(DESCRIPTION)
                .setAuthorName(authorName)
                .setBirth(birth)
                .setNationality(NATIONALITY);
        CreateAuthorRequest request = new CreateAuthorRequest();
        request.setAuthor(authorType);
        AuthorType createAuthorResponse = authorService.createAuthor(request);
        validation.validateCreateAuthor(authorType.getAuthorId(), createAuthorResponse.getAuthorId());
        DeleteAuthorRequest deleteAuthorRequest = new DeleteAuthorRequest();
        deleteAuthorRequest.setAuthorId(AUTHOR_DELETE_ID);
        deleteAuthorRequest.setOptions(new DeleteParams().setForcibly(false));
        String response = authorService.deleteAuthor(deleteAuthorRequest);
        validation.validateDeleteAuthor(response, AUTHOR_DELETE_ID);
    }

    @AfterClass
    public void deleteAuthors() {
        authorService.deleteAuthor(new DeleteAuthorRequest().setAuthorId(AUTHOR_ID));
    }
}
