package ua.com.epam;

import org.testng.annotations.Test;
import ua.com.epam.entities.AuthorType;
import ua.com.epam.entities.Authors;
import ua.com.epam.entities.GetAuthorsRequest;
import ua.com.epam.entities.SearchParamsWithPagination;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ua.com.epam.utils.Constant.*;

public class FiltersAuthorTests extends BaseTest {

    @Test(description = "Verify author size")
    public void checkSizeAuthors() {

        GetAuthorsRequest request = new GetAuthorsRequest();
        SearchParamsWithPagination search = new SearchParamsWithPagination();
        search.setSize(SIZE)
                .setOrderType(DESC)
                .setPagination(true)
                .setPage(BigInteger.valueOf(PAGE));
        request.setSearch(search);

        Authors response = authorService.getAuthors(request);
        List<AuthorType> authors = response.getAuthor();
        validation.validateAuthorsSize(authors.stream().map(AuthorType::getAuthorId).count());
    }

    @Test(description = "Verify author page")
    public void checkPageAuthors() {

        GetAuthorsRequest request = new GetAuthorsRequest();
        SearchParamsWithPagination search = new SearchParamsWithPagination();
        search.setSize(SIZE)
                .setPage(BigInteger.valueOf(PAGE))
                .setOrderType(ASC);
        request.setSearch(search);

        Authors response = authorService.getAuthors(request);
        List<AuthorType> authors = response.getAuthor();
        validation.validateAuthorsSize(authors.stream().map(AuthorType::getAuthorId).count());
    }

    @Test(description = "Verify orderType for author")
    public void checkOrderTypeAsc() {
        GetAuthorsRequest request = new GetAuthorsRequest();
        SearchParamsWithPagination search = new SearchParamsWithPagination();
        search.setSize(SIZE)
                .setOrderType(ASC)
                .setPage(BigInteger.valueOf(PAGE));
        request.setSearch(search);

        Authors response = authorService.getAuthors(request);

        List<Long> actFirstNames = response.getAuthor().stream()
                .map(AuthorType::getAuthorId)
                .collect(Collectors.toList());
        List<Long> expectedFirstNames = actFirstNames.stream().sorted().collect(Collectors.toList());
        validation.validateSorting(actFirstNames, expectedFirstNames, ASC);
    }

    @Test(description = "Verify orderType for author")
    public void checkOrderTypeDesc() {

        GetAuthorsRequest request = new GetAuthorsRequest();
        SearchParamsWithPagination search = new SearchParamsWithPagination();
        search.setSize(SIZE)
                .setOrderType(DESC)
                .setPage(BigInteger.valueOf(PAGE));
        request.setSearch(search);

        Authors response = authorService.getAuthors(request);

        List<Long> actFirstNames = response.getAuthor().stream()
                .map(AuthorType::getAuthorId)
                .collect(Collectors.toList());
        List<Long> expectedFirstNames = actFirstNames.stream()
                .sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        validation.validateSorting(actFirstNames, expectedFirstNames, DESC);
    }

}
