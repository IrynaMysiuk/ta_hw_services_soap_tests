package ua.com.epam.entities;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="genre" type="{libraryService}GenreType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "genre"
})
@XmlRootElement(name = "createGenreResponse")
public class CreateGenreResponse {

    @XmlElement(required = true)
    protected GenreType genre;

    /**
     * Gets the value of the genre property.
     *
     * @return possible object is
     * {@link GenreType }
     */
    public GenreType getGenre() {
        return genre;
    }

    /**
     * Sets the value of the genre property.
     *
     * @param value allowed object is
     *              {@link GenreType }
     */
    public void setGenre(GenreType value) {
        this.genre = value;
    }

}
