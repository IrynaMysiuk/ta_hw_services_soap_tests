package ua.com.epam.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for SearchParamsWithPagination complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SearchParamsWithPagination">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="asc"/>
 *               &lt;enumeration value="desc"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="page">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="pagination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="size">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchParamsWithPagination", propOrder = {
        "orderType",
        "page",
        "pagination",
        "size"
})
public class SearchParamsWithPagination {

    @XmlElement(required = true, defaultValue = "asc")
    protected String orderType;
    @XmlElement(required = true, defaultValue = "1")
    protected BigInteger page;
    @XmlElement(defaultValue = "true")
    protected boolean pagination;
    @XmlElement(defaultValue = "10")
    protected int size;

    /**
     * Gets the value of the orderType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public SearchParamsWithPagination setOrderType(String value) {
        this.orderType = value;
        return this;
    }

    /**
     * Gets the value of the page property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getPage() {
        return page;
    }

    /**
     * Sets the value of the page property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public SearchParamsWithPagination setPage(BigInteger value) {
        this.page = value;
        return this;
    }

    /**
     * Gets the value of the pagination property.
     */
    public boolean isPagination() {
        return pagination;
    }

    /**
     * Sets the value of the pagination property.
     */
    public SearchParamsWithPagination setPagination(boolean value) {
        this.pagination = value;
        return this;
    }

    /**
     * Gets the value of the size property.
     */
    public int getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     */
    public SearchParamsWithPagination setSize(int value) {
        this.size = value;
        return this;
    }

}
