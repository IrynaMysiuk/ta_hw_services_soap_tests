package ua.com.epam.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static ua.com.epam.config.PropConstants.*;

public class DataProp {
    private Properties props = new Properties();

    public DataProp() {
        FileInputStream fis;

        try {
            fis = new FileInputStream("src/main/resources/data.properties");
            props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String apiHost() {
        return props.getProperty(API_HOST);
    }

    public String apiProtocol() {
        return props.getProperty(API_PROTOCOL);
    }

    public int apiPort() {
        return Integer.parseInt(props.getProperty(API_PORT));
    }
}