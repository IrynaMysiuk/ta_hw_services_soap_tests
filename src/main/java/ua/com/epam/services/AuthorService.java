package ua.com.epam.services;

import org.apache.log4j.Logger;
import ua.com.epam.entities.*;

public class AuthorService {
    protected LibraryPort libraryPort;
    protected LibraryPortService service;
    private static Logger log = Logger.getLogger(AuthorService.class);

    public AuthorService() {
        service = new LibraryPortService();
        libraryPort = service.getLibraryPortSoap11();
    }

    public AuthorType getAuthor(GetAuthorRequest request) {
        GetAuthorResponse response = libraryPort.getAuthor(request);
        log.info(response.getAuthor());
        return response.getAuthor();
    }

    public Authors getAuthors(GetAuthorsRequest request) {
        GetAuthorsResponse response = libraryPort.getAuthors(request);
        log.info(response.getAuthors());
        return response.getAuthors();
    }

    public AuthorType createAuthor(CreateAuthorRequest request) {
        CreateAuthorResponse response = libraryPort.createAuthor(request);
        log.info(response.getAuthor());
        return response.getAuthor();
    }

    public String deleteAuthor(DeleteAuthorRequest request) {
        DeleteAuthorResponse response = libraryPort.deleteAuthor(request);
        log.info(response.getStatus());
        return response.getStatus();
    }

}
