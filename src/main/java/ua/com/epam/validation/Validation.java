package ua.com.epam.validation;

import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.List;


public class Validation {

    public void validateAuthorsSize(long authorIds) {
        Assert.assertTrue(authorIds > 0,
                "Incorrect author's size");
    }

    public void validateSorting(List<Long> actFirstNames, List<Long> expectedFirstNames, String orderBy) {
        SoftAssert softAssert = new SoftAssert();
        for (int counter = 0; counter < actFirstNames.size(); counter++) {
            softAssert.assertEquals(actFirstNames.get(counter), expectedFirstNames.get(counter),
                    " Author list is not sorted  by id for " + orderBy);
        }
        softAssert.assertAll();
    }


    public void validateCreateAuthor(long expectedAuthorId, long actualAuthorId) {

        Assert.assertEquals(actualAuthorId, expectedAuthorId,
                "Incorrect author id for creating author!");

    }

    public void validateDeleteAuthor(String actualMessage, long actualAuthorId) {
        Assert.assertEquals(actualMessage, "Successfully deleted author " + actualAuthorId,
                "Incorrect author id for deleting author!");

    }

    public void validateUnknownAuthor(String actualMessage, long actualAuthorId) {
        Assert.assertTrue(actualMessage.contains("Author with id " + actualAuthorId + " not found"),
                "Incorrect message for deleting author!");

    }

    public void validateDuplicateAuthor(String actualMessage, long actualAuthorId) {
        Assert.assertTrue(actualMessage.contains("Author with id " + actualAuthorId + " already exists"),
                "Incorrect message for duplicated authors!");

    }

}
